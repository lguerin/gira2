package gira

class UrlMappings {

    static mappings = {

        "/api/users"(resources: 'user')
        "/api/users/$id/$action" {
            controller = 'user'
            constraints {
                id nullable: false, blank: false
            }
        }

        "/api/userStories/$id/storyPoints/$points"(controller: 'userStory', action: 'storyPoints', method: 'PUT')
        "/api/userStories"(resources: 'userStory')
        "/api/userStories"(resources: 'userStory') {
            "/comments"(resources: 'comment', includes:['index', 'show', 'save', 'delete'])
            collection {
                '/last'(controller: 'userStory', action: 'last')
            }
        }

        "/api/userStories/$id/$action" {
            controller = 'userStory'
            constraints {
                id nullable: false, blank: false
            }
        }

        "/"(controller: 'application', action:'index')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
