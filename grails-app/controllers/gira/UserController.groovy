package gira


import grails.rest.*

class UserController extends RestfulController {

    static responseFormats = ['json', 'xml']

    UserDataService userDataService

    UserController() {
        super(User)
    }

    def hello() {
        User user = userDataService.getUser(params.id)
        render "Hello $user.fullname"
    }
}
