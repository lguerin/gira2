package gira

import gira.types.WorkflowState
import grails.util.Environment
import io.codearte.jfairy.Fairy
import io.codearte.jfairy.producer.person.Person
import org.springframework.beans.factory.annotation.Autowired

class BootStrap {

    @Autowired
    Fairy fairy

    def init = { servletContext ->
        if (Environment.current in [Environment.DEVELOPMENT, Environment.TEST]) {
            this.loadData()
        }
    }

    def destroy = {
    }

    def loadData() {
        log.info ">> Loading data into env: $Environment.current"
        // Create 20 user and user stories
        20.times {
            Person p = fairy.person()
            User u = new User(login: p.username + "_$it", fullname: p.fullName, email: p.email).save()
            UserStory us = new UserStory(title: "us $it", description: fairy.textProducer().sentence(), createdBy: u, storyPoints: it + 1)
            5.times {
                us.addToComments(new Comment(description: fairy.textProducer().sentence(), createdBy: u))
            }
            us.save()
        }

        // Adding some followers
        List<User> allUsers = User.findAll()
        Random random = new Random()
        for (i in 1..5) {
            UserStory us = UserStory.get(i)
            3.times {
                User u = allUsers[random.nextInt(19)]
                us.addToFollowers(u).save()
            }
            // And some assigned users
            us.assignedTo = allUsers[random.nextInt(19)]
            us.state = i % 2 == 0 ? WorkflowState.IN_PROGRESS : WorkflowState.TO_TEST
            us.save()
        }

        log.info "<< done"
    }
}
