package gira

import org.springframework.boot.Banner
import org.springframework.core.env.Environment

import static grails.util.Metadata.current as metaInfo

/**
 * Simple Spring Boot banner implementation
 */
class GiraBanner implements Banner {

    private static final String BANNER = '''
   ______ ____ ____   ___ 
  / ____//  _// __ \\ /   |
 / / __  / / / /_/ // /| |
/ /_/ /_/ / / _, _// ___ |
\\____//___//_/ |_|/_/  |_|
                          
'''

    @Override
    void printBanner(Environment environment, Class<?> sourceClass, PrintStream out) {

        out.println BANNER

        row 'Day ', '2', out
        row 'Grails version', metaInfo.getGrailsVersion(), out
        row 'Groovy version', GroovySystem.version, out
        row 'JVM version', System.getProperty('java.version'), out

        out.println()
    }

    private void row(final String description, final value, final PrintStream out) {
        out.print ':: '
        out.print description.padRight(16)
        out.print ' :: '
        out.println value
    }
}
