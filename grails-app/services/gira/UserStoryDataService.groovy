package gira

import grails.gorm.services.Query
import grails.gorm.services.Service
import grails.gorm.transactions.Transactional

@Service(UserStory)
abstract class UserStoryDataService {

    protected  abstract UserStory getUserStory(Serializable id)

    protected  abstract List<UserStory> findUserStories(Map args)

    @Transactional
    boolean updateStoryPoints(Serializable id, int points) {
        UserStory us = getUserStory(id)
        if (us != null) {
            us.storyPoints = points
            us.save()
            return true
        }
        return false
    }
}
