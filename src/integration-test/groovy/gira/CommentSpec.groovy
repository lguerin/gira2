package gira

import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.time.TimeCategory
import spock.lang.Specification

@Integration
@Rollback
class CommentSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "should filter last created comments"() {
        setup:
        UserStory us = UserStory.get(1)
        Date createdAt
        use (TimeCategory) {
            createdAt = new Date() - 30.minutes
        }
        Comment c = new Comment(description: 'xxx')
        c.dateCreated = createdAt
        us.addToComments(c).save(flush: true)

        when:
        List<Comment> list = Comment.lastMinutesComments(40, us.state)

        then:
        list.size() > 1
    }
}
