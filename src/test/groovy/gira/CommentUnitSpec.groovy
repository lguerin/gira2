package gira

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class CommentUnitSpec extends Specification implements DomainUnitTest<Comment> {

    def setup() {
    }

    def cleanup() {
    }

    void "should persist user story instances"() {
        setup:
        Fixtures.buildUserStoryWithComments()

        expect:
        Comment.count() == 2
    }
}
