package gira

import grails.test.hibernate.HibernateSpec
import spock.lang.Shared

class UserDataServiceSpec extends HibernateSpec {

    @Shared
    UserDataService userDataService

    def setup() {
        userDataService = hibernateDatastore.getService(UserDataService)
    }

    def cleanup() {
    }

    void "should find a user from its login"() {
        setup:
        Fixtures.buildUser([login: 'lguerin', email: 'lguerin@ifap.nc'])

        when:
        User u = userDataService.getUser('lguerin')

        then:
        u != null
        u.login == 'lguerin'
    }

    void "should delete users from mail pattern"() {
        setup:
        Fixtures.buildUser([login: 'lguerin', email: 'lguerin@ifap.nc'])

        when:
        userDataService.delete("%ifap%")
        List<User> users = User.findAll()

        then:
        users.size() == 0
    }

    void "should get list of users"() {
        setup:
        Fixtures.buildUser()
        Fixtures.buildUser()

        when:
        List<User> users = userDataService.findAll()

        then:
        users.size() == 2
    }
}
