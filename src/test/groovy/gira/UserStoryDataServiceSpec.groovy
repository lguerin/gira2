package gira

import grails.test.hibernate.HibernateSpec
import spock.lang.Shared

class UserStoryDataServiceSpec extends HibernateSpec {

    @Shared
    UserStoryDataService userStoryDataService

    def setup() {
        userStoryDataService = hibernateDatastore.getService(UserStoryDataService)
    }

    def cleanup() {
    }

    void "should retrieve a user story"() {
        setup:
        Fixtures.buildUserStory()

        when:
        UserStory us = userStoryDataService.getUserStory(1)

        then:
        us != null
        us.createdBy != null
        us.assignedTo != null
        us.followers?.size() >= 0
    }

    void "should retrieve all user stories with params"() {
        setup:
        Fixtures.buildUserStory()
        Fixtures.buildUserStory()
        Fixtures.buildUserStory()

        when:
        List<UserStory> userStories = userStoryDataService.findUserStories()

        then:
        userStories.size() == 3

        when:
        userStories = userStoryDataService.findUserStories([max: 1])

        then:
        userStories.size() == 1
    }

    void "should update story points of a user story"() {
        setup:
        UserStory us = Fixtures.buildUserStoryWithComments()

        when:
        def success = userStoryDataService.updateStoryPoints(us.id, 5)
        us = UserStory.get(us.id)

        then:
        success == true
        us != null
        us.storyPoints == 5
        us.comments?.size() == 2
    }
}
